import {iFrameRoute} from "../helpers/apiPath";

export class iFramePage {
    static IFRAME_ID = "#mce_0_ifr";

    static visit = () => {
        cy.visit(iFrameRoute());
    }
}