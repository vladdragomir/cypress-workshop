import {uploadRoute} from "../helpers/apiPath";

export class UploadPage {
    static HEADLINE = "h3";
    static FILE_UPLOAD_INPUT = "#file-upload";
    static UPLOAD_BTN = "[type=submit]";
    static UPLOADED_FILES_SECTION = "[id=uploaded-files]";

    static EXPECTED_HEADLINE_TEXT = "File Uploaded!";
    static EXPECTED_UPLOADED_FILE_TEXT = "\n    example.json\n  ";

    static visit = () => {
        cy.visit(uploadRoute());
    }

    static uploadFile = (locator, file) => {
        cy.get(locator).attachFile(file);
        cy.get(UploadPage.UPLOAD_BTN).click();
    }
}