import {loginRoute} from "../helpers/apiPath";

export class LoginPage {
    static ALERT_MESSAGE = "#flash";
    static HEADLINE = "h2";
    static SUBHEADER = ".subheader";
    static USERNAME_LABEL = "[for=username]";
    static PASSWORD_LABEL = "[for=password]";
    static USERNAME_INPUT = "#username";
    static PASSWORD_INPUT = "#password";
    static SUBMIT_BTN = "[type=submit]";

    static WRONG_USERNAME_ALERT_MESSAGE = "\n            Your username is invalid!\n            ×\n          ";
    static WRONG_PASSWORD_ALERT_MESSAGE = "\n            Your password is invalid!\n            ×\n          ";


    static MAIN_ELEMENTS = [
        LoginPage.HEADLINE,
        LoginPage.SUBHEADER,
        LoginPage.USERNAME_LABEL,
        LoginPage.PASSWORD_LABEL,
        LoginPage.USERNAME_INPUT,
        LoginPage.PASSWORD_INPUT,
        LoginPage.SUBMIT_BTN
    ]

    static visit = () => {
        cy.visit(loginRoute());
    }

    static checkElementsVisibility = () => {
        LoginPage.MAIN_ELEMENTS.forEach(element => {
            cy.get(element).should("be.visible");
        });
    }

    static fillInLoginForm = (username, password) => {
        cy.get(LoginPage.USERNAME_INPUT).clear().type(username);
        cy.get(LoginPage.PASSWORD_INPUT).clear().type(password);
        cy.get(LoginPage.SUBMIT_BTN).click();
    }
}