import {welcomePageRoute} from "../helpers/apiPath";

export class WelcomePage {
    static EXPECTED_PAGE_TITLE = "Product Development Services | 3Pillar Global";
    static LOGO = ".main-logo-link > img";

    static MAIN_ELEMENTS  = [
        WelcomePage.LOGO
    ]

    static visit = () => {
        cy.visit(welcomePageRoute());
    }

    static checkElementsVisibility = () => {
        WelcomePage.MAIN_ELEMENTS.forEach(element => {
            cy.get(element).should("be.visible");
        });
    }
}