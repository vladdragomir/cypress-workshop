export class SecurePage {
    static ALERT_MESSAGE = "#flash";
    static HEADLINE = "h2";
    static SUBHEADER = ".subheader";
    static LOGOUT_BTN = "[href='/logout']";

    static EXPECTED_HEADLINE_TEXT = " Secure Area";
    static EXPECTED_SUBHEADER_TEXT = "Welcome to the Secure Area. When you are done click logout below.";
    static ALERT_MESSAGE_EXPECTED_TEXT = "\n            You logged into a secure area!\n            ×\n          ";


    static MAIN_ELEMENTS = [
        SecurePage.HEADLINE,
        SecurePage.SUBHEADER,
        SecurePage.ALERT_MESSAGE,
        SecurePage.LOGOUT_BTN
    ]

    static checkElementsVisibility = () => {
        SecurePage.MAIN_ELEMENTS.forEach(element => {
            cy.get(element).should("be.visible");
        });
    }
}