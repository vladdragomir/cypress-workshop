const baseConfig = require("./base.config");

module.exports = {
    ...baseConfig,
    baseUrl: "http://the-internet.herokuapp.com/",
    env: {
        BASE_API_URL: "https://postman-echo.com/"
    }
}