const baseConfig = require("./base.config");

module.exports = {
    ...baseConfig,
    baseUrl: "remote_url_here",
    env: {
        BASE_API_URL: "https://postman-echo.com/"
    }
}