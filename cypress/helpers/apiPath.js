export const welcomePageRoute = () => 'https://www.3pillarglobal.com/';
export const loginRoute = () => 'login';
export const uploadRoute = () => 'upload';
export const iFrameRoute = () => 'iframe';