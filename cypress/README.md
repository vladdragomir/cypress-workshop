# The Testing Framework

This directory contains the source code used for testing applications with Cypress.
### Prerequisites

Only one,  [Node.js](https://nodejs.org/en/download/). 

### Installation

After installing Node.js and cloning the repository, run `yarn install` or `npm install` in the `cypress-workshop/cypress` directory.

### Running the tests

`yarn cypress open` - to open the Test Runner and run the tests manually

`yarn cypress run` - to run all the tests in headless mode
