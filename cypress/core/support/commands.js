// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//

const BASE_API_URL = Cypress.env("BASE_API_URL");

Cypress.Commands.add("getIframeBody", (iFrameLocator) => {
    return cy.get(iFrameLocator).its('0.contentDocument.body').then(cy.wrap);
});

Cypress.Commands.add('iframe', {prevSubject: 'optional'}, (iframe = false, subject, waitFor = 'body', timeout = 20000) => {
    const context = iframe ? cy.wrap(iframe).find(subject, {timeout}) : cy.get(subject, {timeout});
    return context
        .should(frame => {
            expect(frame.contents().find(waitFor)).to.exist
        })
        .then(frame => {
            return frame.contents().find('body');
        });
});

Cypress.Commands.add("getRequest", (endpoint, useAlias) => {
    const props = {
        method: 'GET',
        url: BASE_API_URL + endpoint,
        failOnStatusCode: false,
        auth: {
            username: "postman",
            password: "password"
        }
    };
    if(useAlias) {
        cy.request(props).as("getRequest");
    } else {
        cy.request(props);
    }
});

Cypress.Commands.add("postRequest", (endpoint, payload, useAlias) => {
    const props = {
        method: 'POST',
        url: BASE_API_URL + endpoint,
        failOnStatusCode: false,
        auth: {
            username: "postman",
            password: "password"
        },
        body: payload
    };
    if(useAlias) {
        cy.request(props).as("postRequest");
    } else {
        cy.request(props);
    }
});