import {UploadPage} from "../../pages/UploadPage";

describe("Upload Page", () => {
    beforeEach(() => {
        UploadPage.visit();
    })

    it('should test that uploading a file works', () => {
        const fileName = "example.json";
        UploadPage.uploadFile(UploadPage.FILE_UPLOAD_INPUT, fileName);
        cy.get(UploadPage.HEADLINE).should("have.text", UploadPage.EXPECTED_HEADLINE_TEXT);
        cy.get(UploadPage.UPLOADED_FILES_SECTION).should("have.text", UploadPage.EXPECTED_UPLOADED_FILE_TEXT);
    });
});