describe("Requests with Cypress", () => {
    it('should make a GET request', () => {
        cy.getRequest("get?foo1=bar1").then(response => {
            console.log(response.body.args);
        });
    });

    it('should make a GET request with Aliases', () => {
        cy.getRequest("get?foo1=bar1", true);
        cy.get("@getRequest").should(response => {
            console.log(response.body.args);
        });
    });

    it('should make a GET request for authentication', () => {
        cy.getRequest("basic-auth").then(response => {
            expect(response.body).to.deep.equal({authenticated: true});
        });
    });

    it('should make a POST request', () => {
        let payload = {
            foo1: "bar1",
            hand: "wave"
        };
        cy.postRequest("post", payload).then(response => {
            console.log(response.body.data);
            expect(response.body.data).to.deep.equal({foo1: "bar1", hand: "wave"});
        });
    });
})