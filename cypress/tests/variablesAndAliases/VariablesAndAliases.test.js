import {loginRoute} from "../../helpers/apiPath";
import {LoginPage} from "../../pages/LoginPage";

describe("Variables with Sync and Async code", () => {
    it("this example will not work as expected", () => {
        let headline;     // evaluates immediately as undefined
        console.log(`After declaration: ${headline}`)
        cy.visit(loginRoute()) // Nothing happens yet
        cy.get(LoginPage.HEADLINE)        // Still, nothing happens yet
            .then(($element) => {          // Nothing happens yet
                // this line evaluates after the .then executes
                headline = $element.text();
                console.log(`Inside the then(): ${headline}`)
            });

        // this evaluates before the .then() above
        // so the headline is still undefined
        if (headline==='Login Page') {             //headline is undefined
            console.log(`I'm inside the if, doing my thing: ${headline}`);
        } else {
            // this will run because headline is undefined
            console.log(`I'm inside the else, doing my other thing: ${headline}`);
        }
    });

    it("this example will work as expected", () => {
        let headline;     // evaluates immediately as undefined
        console.log(`After declaration: ${headline}`)
        cy.visit(loginRoute()) // Nothing happens yet
        cy.get(LoginPage.HEADLINE)
            .then(($element) => {
                // this line evaluates after the .then() executes
                headline = $element.text();
                console.log(`Inside the then(): ${headline}`)
                // this line evaluates after the .then() executes so the value is Login Page
                if (headline==='Login Page') {             //headline is now Login Page
                    console.log(`I'm inside the if, doing my thing: ${headline}`);
                } else {
                    // this will not run because headline is Login Page
                    console.log(`I'm inside the else, doing my other thing: ${headline}`);
                }
            });
    });
});

describe("Aliases in beforeEach()", () => {
    beforeEach(() => {
        cy.visit(loginRoute());
        const headline = cy.get(LoginPage.HEADLINE);

        //cy.get(LoginPage.HEADLINE).invoke('text').as("headline");
    });


    it('it will not access variables declared in beforeEach()', () => {
        //does not have access to headline
        //declaring let outside of the beforeEach will work but there is a more elegant way
        console.log(headline);
    });

    it('it will have access variables declared in beforeEach() using Aliases', function () {
        console.log(this.headline);
    });

});

describe("Aliases to find elements", () => {
    it('this will not work',  () => {
        cy.visit(loginRoute());
        const headline = cy.get(LoginPage.HEADLINE);
        cy.reload();
        headline.should("have.text", "Login Page");
    });

    it('this will work with Aliases',  () => {
        cy.visit(loginRoute());
        cy.get(LoginPage.HEADLINE).as('headline');
        cy.reload();
        cy.get('@headline').should("have.text", "Login Page");
    });
});