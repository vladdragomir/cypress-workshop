import {LoginPage} from "../../pages/LoginPage";
import {SecurePage} from "../../pages/SecurePage";

describe("Login Page", () => {
    beforeEach(() => {
        LoginPage.visit();
    });

    it('a. should check elements visibility', () => {
        LoginPage.checkElementsVisibility();
    });

    it('b. should check that the login works with correct credentials', () => {
        LoginPage.fillInLoginForm("tomsmith", "SuperSecretPassword!");
        cy.get(SecurePage.ALERT_MESSAGE).should("have.text", SecurePage.ALERT_MESSAGE_EXPECTED_TEXT);
        SecurePage.checkElementsVisibility();

        cy.get(SecurePage.HEADLINE).should("have.text", SecurePage.EXPECTED_HEADLINE_TEXT);
        cy.get(SecurePage.SUBHEADER).should("have.text", SecurePage.EXPECTED_SUBHEADER_TEXT);
        cy.get(SecurePage.ALERT_MESSAGE).should("have.text", SecurePage.ALERT_MESSAGE_EXPECTED_TEXT);
    });

    it('c. should test the error message when the username is invalid', () => {
        LoginPage.fillInLoginForm("tomsmiths", "SuperSecretPassword!");
        cy.get(LoginPage.ALERT_MESSAGE).should("have.text", LoginPage.WRONG_USERNAME_ALERT_MESSAGE);
    });

    it('d. should test the error message when the password is invalid', () => {
        LoginPage.fillInLoginForm("tomsmith", "SuperSecretPassword!!");
        cy.get(LoginPage.ALERT_MESSAGE).should("have.text", LoginPage.WRONG_PASSWORD_ALERT_MESSAGE);
    });
});