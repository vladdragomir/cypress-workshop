import {WelcomePage} from "../../pages/WelcomePage";

describe("Welcome Page", () => {
    beforeEach(() => {
        WelcomePage.visit();
    })
    it('should check elements visibility', () => {
        WelcomePage.checkElementsVisibility();
    });
    it('should check page title', function () {
        cy.title().should("equal", WelcomePage.EXPECTED_PAGE_TITLE);
    });
});