import {iFramePage} from "../../pages/iFramePage";

describe("iFrame Page", () => {
    beforeEach(() => {
        iFramePage.visit();
    })

    it('should test that typing inside an iFrame works with the getIframeBody() method', () => {
        cy.getIframeBody(iFramePage.IFRAME_ID).clear().type("Testing inside an iFrame");
    });

    it('should test that typing inside an iFrame works with the iframe() method', () => {
        cy.iframe(iFramePage.IFRAME_ID).clear().type("Testing inside an iFrame");
    });
});

describe("iFrame in iFrame", () => {
    it('should test that working with iFrames in iFrames works',  () => {
        cy.on('uncaught:exception', () => {
            done();
            return false
        });
        // https://chercher.tech/practice/frames-example-selenium-webdriver has an uncaught exception causing cypress
        // to stop running the tests. That's why we need the code above, to bypass the exception.

        cy.visit("https://chercher.tech/practice/frames-example-selenium-webdriver");
        cy.iframe("#frame1").iframe("#frame3").find("#a").check();
        cy.iframe("#frame2").find("#animals").select("Baby Cat");
    });
});