# cypress-workshop

This project contains the code and exercises from the QA CoP - End-to-end testing with Cypress.io Workshop.

# Testing Framework
#### [Source code](https://gitlab.com/vladdragomir/cypress-workshop/-/tree/master/cypress)

# Exercises

#### [Session 1](https://gitlab.com/vladdragomir/cypress-workshop/-/tree/master/exercices/session1)

#### [Session 2](https://gitlab.com/vladdragomir/cypress-workshop/-/tree/master/exercices/session2)

#### [Session 3](https://gitlab.com/vladdragomir/cypress-workshop/-/tree/master/exercices/session3)
