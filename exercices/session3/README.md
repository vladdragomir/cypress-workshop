# Session 3 - Exercises

### Exercise 1

Create a new test file and a new page object model for the http://the-internet.herokuapp.com/login page. Write 4 tests that cover the following scenarios:

*  Test that all the visual elements are displayed
*  Test that the login works with the correct credentials
*  Test the error message when the username is invalid
*  Test the error message when the password is invalid


### Exercise 2

Implement a test that navigates to http://the-internet.herokuapp.com/upload and uploads the `example.json` file located in the `fixtures` folder.

>  Useful resources: https://www.npmjs.com/package/cypress-file-upload

### Exercise 3

Implement a test that navigates to http://the-internet.herokuapp.com/iframe and types “Testing inside an iFrame” in the text editor:

![image](/uploads/a005c52c0c29731329e8f294ba46d006/image.png)

**Note**: before typing inside the editor, we need to switch the focus to the iframe that contains the editor. 

>  Useful resources: 
  https://www.cypress.io/blog/2020/02/12/working-with-iframes-in-cypress/ 
  and https://www.npmjs.com/package/cypress-iframe
