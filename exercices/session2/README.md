# Session 2 - Exercises

### Coding
1. Write a function that takes a nr as input and will resolve 2 sec later with the number doubled.
   Then run a few different values through our function and add the result? (for ex: 10, 20, 30 - displayed sum will be 130) 
2. Rewrite the previous code using async/await

### --- Quiz ---
#1. What is the output of the following code:
```
function job() {
    return new Promise(function(resolve, reject) {
        reject();
    });
}
let promise = job();
promise.then(function() {
    console.log('Success 1');
}).then(function() {
    console.log('Success 2');
}).then(function() {
    console.log('Success 3');
}).catch(function() {
    console.log('Error 1');
}).then(function() {
    console.log('Success 4');
});
```
#2. What is the output of the following code:
```
function job(state) {
    return new Promise(function(resolve, reject) {
        if (state) {
            resolve('success');
        } else {
            reject('error');
        }
    });
}
let promise = job(true);
promise.then(function(data) {
    console.log(data);
    return job(false);
}).catch(function(error) {
    console.log(error);
    return 'Error caught';
}).then(function(data) {
    console.log(data);
    return job(true);
}).catch(function(error) {
    console.log(error);
});
```
#3. What is the output of the following code and why:
```
let x = 0;
let test = async () => {
  x += await 2;
  console.log(x);
}
test();
x += 1;
console.log(x);
```