// 1. Write a function that takes a nr as input and will resolve 2 sec later with the number doubled.
//    Then run a few different values through our function and add the result? (for ex: 10, 20, 30 - displayed sum will be 130)

let doubleAfter2Seconds = (x) => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(x * 2);
        }, 2000);
    });
};

let addPromise = (x) => {
    return new Promise(resolve => {
        doubleAfter2Seconds(10).then((a) => {
            doubleAfter2Seconds(20).then((b) => {
                doubleAfter2Seconds(30).then((c) => {
                    resolve(x + a + b + c);
                })
            })
        })
    });
};

addPromise(10).then((sum) => {
    console.log(sum);
});

// 2. Rewrite the previous code using async/await.

let doubleAfter2Seconds= (x) => {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(x * 2);
        }, 2000);
    });
};

let addAsync = async(x) => {
    const a = await doubleAfter2Seconds(10);
    const b = await doubleAfter2Seconds(20);
    const c = await doubleAfter2Seconds(30);
    return x + a + b + c;
};

addAsync(10).then((sum) => {
    console.log(sum);
});

// 3. Quiz

// 3.1
// Error 1
// Success 4

// 3.2
// success
// error
// Error caught

// 3.3
// 1
// 2