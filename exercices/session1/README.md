# Session 1 - Exercices

#### 1. Write a JavaScript program to determine if a sentence is a pangram.

*A sentence containing all the letters of the alphabet is known as a pangram.*

Eg.: "The quick brown fox jumps over the lazy dog."

#### 2. Given an age in seconds, write a program to calculate how old someone would be on another planet.

	Planet’s seconds:
		Mercury: 0.2408467,
 		Venus: 0.61519726,
  		Earth: 1.0, earthSeconds = 31557600
 		Mars: 1.8808158,
 		Jupiter: 11.862615,
 		Saturn: 29.447498,
  		Uranus: 84.016846,
  		Neptune: 164.79132


*The formula: **seconds / (earthSeconds * planetSec)***

So, if you were told someone were 1,000,000,000 seconds old, you should be able to say that they're 31.69 Earth-years old.


#### 3. Compute Pascal's triangle up to a given number of rows.

*In Pascal's Triangle each number is computed by adding the numbers to the right and left of the current position in the previous row.*  

https://upload.wikimedia.org/wikipedia/commons/0/0d/PascalTriangleAnimated2.gif

#### 4. Write a JavaScript program to find the most frequent item of an array.

Sample Array : let array=[3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 2, 4, 9, 3];

Sample Output : a (5 times)

