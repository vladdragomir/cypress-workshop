// 1. Determine if a sentence is a pangram.
let alphabet = "abcdefghijklmnopqrstuvwxyz".split('');

let str = "qwertyuiopasdfghjklzxcvbnm";

let isPangram = (string) => {
    let pangram = true;
    for (let item of alphabet) {
        if (!str.toLowerCase().includes(item)) pangram = false;
    }
    return pangram
};

isPangram(str);

// 2. Given an age in seconds, calculate how old someone is in terms of a given planet's solar years.
const YEAR_MULTIPLIER = {
    earth: 1,
    mercury: 0.2408467,
    venus: 0.61519726,
    mars: 1.8808158,
    jupiter: 11.862615,
    saturn: 29.447498,
    uranus: 84.016846,
    neptune: 164.79132
};
const EARTH_YEAR_IN_SECONDS = 31557600;
let myAge = 1104516000;

const age = (planet, myAge) => {
    return (myAge / (planet * EARTH_YEAR_IN_SECONDS)).toFixed(2);
};

age(YEAR_MULTIPLIER.earth, myAge);

// 3. Compute Pascal's triangle up to a given number of rows.
const rows = (n) => {
    if (n <= 0) return [];

    let result = [[1]];
    for (let i = 1; i < n; i++) {
        result.push(nextRow(result[i - 1]))
    }

    return result
};

const nextRow = (row) => {
    let next = [];

    next.push(row[0]);
    for (let i = 1; i < row.length; i++) {
        next.push(row[i - 1] + row[i])
    }
    next.push(row[row.length - 1]);

    return next
};

rows(5);

// 4. Write a JavaScript program to find the most frequent item of an array.
const input = [3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 3, 'a', 3];
let mostFrequent = 1;
let m = 0;
let item;
for (let i = 0; i < input.length; i++) {
    for (let j = i; j < input.length; j++) {
        if (input[i] == input[j])
            m++;
    }
    if (mostFrequent < m) {
        mostFrequent = m;
        item = input[i];
    }
    m = 0;
}
console.log(`Most frequent item: ${item} (${mostFrequent} times) `);